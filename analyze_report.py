# encoding:utf-8

#
#  IMPORT
#
import copy
import os
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys

from pathlib import Path
from matplotlib import rcParams, font_manager

#
# FONT SETTING
#

# MEMO: グラフの日本語表示のためフォント設定が必要
#       利用環境に合わせて好きなフォントを使用してください。
# MEMO: このサンプルではすもももじ様のけいふぉんと！を利用させていただいております。
#       http://font.sumomo.ne.jp/font_1.html
font_manager._rebuild()
plt.rcParams['font.family'] = 'keifont'

#
# CONST DEFINE
#
ROOT_DIR = os.path.dirname(__file__)
TESTDATA_DIR = ROOT_DIR + os.sep + 'test_data' + os.sep 
RESULt_DIR   = ROOT_DIR + os.sep + 'result'    + os.sep

TEST_FILE_LIST = [
    TESTDATA_DIR + '201910_エクストラ_シティ_custom.csv',
    TESTDATA_DIR + '201910_エクストラ_ポケカの日_custom.csv'
]


#
# METHOD DEFINE
#

# public_buttle_dfからデッキ利用数を数える関数
def count_use_deck( public_buttle_df ):
    if(len(public_buttle_df)==0):
        return pd.Series()
    count1 = public_buttle_df['use'].value_counts()
    count2 = public_buttle_df['opponent'].value_counts()
    total_count = count1.add(count2, fill_value=0)
    return total_count.astype('Int32').sort_values(ascending=False)

# 勝敗履歴のDataFrame(csvファイルのフォーマット)　から勝敗表（数）のDataFrameを作成する。
# deck_list ... 出現するデッキのリスト
# df_list   ... 勝敗履歴のDataFrame
def create_winlose_count_df( deck_list, df_list ):
    _zeros = np.zeros((len(deck_list), len(deck_list)))    
    wl_count_df = pd.DataFrame( data=_zeros, index=deck_list, columns=deck_list)

    for df in df_list:
        for row in df.itertuples():
            _deck_name = row.use
            if _deck_name == _deck_name: # check NaN
                use = _deck_name
            
            wl_count_df.at[use, row.opponent] += int(row.win)
            wl_count_df.at[row.opponent, use] += int(row.lose)

    return wl_count_df

# 勝敗表（数）のDataFrameから勝率のDataFrameを作成する
# df_list   ... 勝敗履歴のDataFrame
def create_winlose_rate_df( wl_count_df ):
    deck_list = wl_count_df.index.values

    _zeros = np.zeros((len(deck_list), len(deck_list)))
    wl_rate_df  = pd.DataFrame( data=_zeros, index=deck_list, columns=deck_list)

    for use in deck_list:
        for opp in deck_list:
            all_count = wl_count_df.loc[use, opp] + wl_count_df.loc[opp, use] 
            wl_rate_df.loc[use, opp] = 0.5 if(all_count<1)else ( wl_count_df.loc[use, opp]/all_count )
    return wl_rate_df


# デッキ利用率円グラフを表示する
def show_use_rate_circle_graph(use_deck_series):
    total_use = np.sum( np.array(use_deck_series.values) )
    labels = [ use_deck_series.index[i] + "[" + str( int(use_deck_series.values[i]/total_use*1000)/10 ) + "%]" for i in range(len(use_deck_series)) ]
    patches, texts = plt.pie(use_deck_series.values, labels=labels, counterclock=False, startangle=90)
    #for t in texts:
    #    t.set_horizontalalignment('center')
    plt.show()


# 勝率表を加工して、勝率ランク表を作成する
def create_rank_df(wl_rate_df):
    ret_df = copy.deepcopy(wl_rate_df)

    count_row = ret_df.iloc[-1]
    weighted_win_rate_list = [0.0] * len(ret_df)
    for i, row in enumerate(ret_df.itertuples()):
        if (row.Index=='count'):
            break
        weighted_win_rate_list[i] = np.sum(row[1:] * count_row.values) / np.sum(count_row.values)

    ret_df = ret_df.assign(weighted_win_rate=weighted_win_rate_list)
    ret_df = ret_df.assign(rank=ret_df.loc[:, 'weighted_win_rate'].rank(ascending=False) )
    return ret_df


if __name__ == "__main__":

    if len(sys.argv) >= 2:
        analyze_file_list = sys.argv[1:]
    else:
        analyze_file_list = TEST_FILE_LIST

    # すべてのファイルをDFに読み出し
    df_list = [ pd.read_csv(csv_filepath) for csv_filepath in analyze_file_list ]

    # １つのDFにまとめる
    buttle_df = pd.concat( df_list )

    # デッキ使用率グラフを表示
    use_deck_series = count_use_deck( buttle_df )
    show_use_rate_circle_graph(use_deck_series)


    # 勝率表を作成＆ファイル保存する
    deck_list = use_deck_series.index.values

    wl_count_df = create_winlose_count_df(deck_list, [buttle_df])
    wl_rate_df  = create_winlose_rate_df( wl_count_df )

    os.makedirs(RESULt_DIR, exist_ok=True)
    outfilepath = RESULt_DIR + 'result_winrate.csv'
    wl_rate_df.to_csv(outfilepath)

    # 特殊処理。超越を削除する
    # use_deck_series.name = 'count'
    # use_deck_series['超越']=0


    # 勝率表を加工して、勝率ランク表を作成する
    rank_df = create_rank_df(wl_rate_df)
    outfilepath = RESULt_DIR + 'result_rank.csv'
    rank_df.to_csv(outfilepath)