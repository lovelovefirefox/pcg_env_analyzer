# pcg_env_analyzer

ポケモンカードゲーム（PCG）の対戦ログから   
メタ環境を予測するPythonスクリプトです。  

**環境：**  
* Python3.6  
* numpy, pandas, matplotlib, seaborn  
* 日本語などのマルチバイト文字を表示させる場合は、対応したフォント

**使い方：**  

1. サンプル動作をさせる場合  

analyze_report.pyを実行するとサンプルデータを使った解析を行います。

    $ python analyze_report.py


使用率グラフが表示され、resultフォルダに解析結果のcsvが出力されます。

2. 任意の対戦レポートを使用する場合  

引数に連続してファイル指定してください。

    $ python analyze_report.py report1.csv, report2.csv report3.csv 

対戦レポートの書式は、test_dataフォルダ内のサンプルファイルを参考にしてください。

**日本語表記**

グラフの日本語表示のためフォント設定が必要です。  
利用環境に合わせて好きなフォントをanalyze_report.pyのソースファイルに設定してください。  
このサンプルではすもももじ様のけいふぉんと！を利用させていただいております。ありがとうございます。  
http://font.sumomo.ne.jp/font_1.html

---

This Python script is for analyzing pcg meta environment from battle reports.  

**REQUIRED:**
* Python3.6  
* numpy, pandas, matplotlib, seaborn  
* Corresponding multi-byte font(When displaying multi-byte characters such as Japanese)

**HOW TO USE:**

1. Sample operation

When analyze_report.py is run, it uses sample data.

    $ Python analyze_report.py

A usage graph is displayed and  analyzed csv file is output to the results folder.

2. Using your reports file

Specify files consecutively in the argument.

    $ Python analyze_report.py report1.csv, report2.csv report3.csv

Refer to the sample file's format in the test_data folder
